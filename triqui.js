class Triqui {

    constructor() {

        this.matriz = [
            ["A", "B", "C"],
            ["D", "E", "F"],
            ["G", "H", "I"]
        ];
        this.dibujarTablero();
        this.definirJugador();


    }

    dibujarTablero() {

        //Crear un objeto que referencie a la división tablero
        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML +
                "<input type='text' id='casilla" + (i + 1) + "' class='casilla' onclick='miTriqui.realizarJugada()'>";

            if ((i + 1) % 3 == 0) {
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }

        }
    }

    definirJugador() {

        let n;

        let miTurno = document.getElementById("turno");
        n = Math.round(Math.random() + 1);

        if (n === 1) {
            this.turno = "X";
        } else {
            this.turno = "O";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }

    realizarJugada() {

        let miElemento = event.target;

        if (!(miElemento.value === "X" || miElemento.value === "O")) {
            miElemento.value = this.turno;
            this.modificarMatriz(miElemento.id);

            let resultado=this.verificarTriqui();
     
            if(resultado===true){

                console.warn("Hubo triqui!!");
                alert("Gano el Jugador : " + this.turno + '  FELICITACIONES!!!');


            }else{
                this.cambiarTurno();
            }
            
        } else {

            console.error("La casila está llena!!");

        }
        

    }



    verificarTriqui() {

        let triqui = false;

        

        for (let fila = 0; fila < 3; fila++) {
            if (this.matriz[fila][0] === this.matriz[fila][1] && this.matriz[fila][0]=== this.matriz[fila][2]) {
                triqui = true;
                return triqui;
            }
        }

        for (let columna = 0; columna < 3; columna++) {
            if (this.matriz[0][columna] === this.matriz[1][columna] && this.matriz[0][columna]=== this.matriz[2][columna]) {
                triqui = true;
                return triqui;
            }
        }

        if (this.matriz[0][0] === this.matriz[1][1] && this.matriz[0][0]=== this.matriz[2][2]) {
            triqui = true;
            return triqui;
        }

        if (this.matriz[0][2] === this.matriz[1][1] && this.matriz[0][2] === this.matriz[2][0]) {
            triqui = true;
            return triqui;
        }


        return triqui;
    }

    


    modificarMatriz(id) {
        let modificables = 
        [       
            ["casilla1","casilla2","casilla3"],
            ["casilla4","casilla5","casilla6"],
            ["casilla7","casilla8","casilla9"]
        ];


        for(let i=0;i<3;i++){
            for(let j=0;j<3;j++){
                if (id===modificables[i][j]){
                    this.matriz[i][j]= this.turno;
                }
            }
        }
    }
    

    cambiarTurno() {

        let miTurno = document.getElementById("turno");

        if (this.turno === "X") {
            this.turno = "O";
        } else {
            this.turno = "X";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;

    }

    borrar(){
        Triqui.clearRect(0, 0,window.innerWidth,window.innerHeight);  
     }

}
